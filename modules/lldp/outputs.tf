/*
    Copyright (c) 2020 World Wide Technology, LLC
    All rights reserved.
    
    author: Joel W. King  @joelwking

    usage:

      Output values are the return values of a Terraform module or submodule.
      This child module uses outputs to expose a resource attributes to the parent module. 

    references:
      - https://www.terraform.io/docs/configuration/modules.html#accessing-module-output-values

*/
#
#   Note variables are case sensitive, we reference this output in the parent main module `../outputs.tf`.
#
output "LLDP_ID" {
  value = aci_lldp_interface_policy.lldp_policy.id
  description = "The Dn of the LLDP policy"
}
