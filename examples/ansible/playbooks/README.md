README.md
---------

The playbook `terraform_main.yml` is an example of using an Ansible playbook to execute Terraform and access the returned output in subsequent tasks in the playbook.

To execute this playbook, first configure the environment. Assume you have a Ubuntu VM. Create a Python virtual environment
```bash
$ sudo apt install python3-venv
$ python3 -m venv ansible210
$ source ansible210/bin/activate
$ pip3 install --upgrade pip
```

Now install Ansible:

```bash
$ pip3 install ansible==2.10.7.0
```

If you are using the remote Terraform backend, you will need to verify it has been initialized. Go to the main (root) directory, assuming you have cloned the repository from your home directory and have configured the S3 backend and created the `credentials_backend` file with the access and secret key.

```bash
$ cd ~/terraform_aci/
$ terraform init -backend-config=credentials_backend
```

The playbook assumes you have specified the APIC credential (password) and the APIC URL as an environment variable.

```bash
$ export TF_VAR_apic_password='changeme'
$ export TF_VAR_apic_url='https://823390b2ee9b.ngrok.io'
```

Verify the APIC username is correctly specified in `credentials.auto.tfvars`. 

>Note: if you encounter an error message which contains the string: `Error: invalid character '<' looking for beginning of value\n\n` the problem will likely be an incorrect URL, username or password.


Make the playbook directory your working directory:

```bash
$ cd ~/terraform_aci/examples/ansible/playbooks
```

Now execute the playbook. Specify the Terraform root directory `path` and optionally the `state` to apply (present) or destroy (absent).

```bash
$ ansible-playbook terraform_main.yml -e 'path=~/terraform_aci state=present' -v
```

The output from the playbook should look similar to the following:
```bash
Using /etc/ansible/ansible.cfg as config file
[WARNING]: provided hosts list is empty, only localhost is available. Note that the implicit localhost does not match 'all'

PLAY [Execute the main Terraform module] ***************************************************************************************************

TASK [Invoke Terraform] ********************************************************************************************************************
ok: [localhost] => {"changed": false, "command": "/usr/local/bin/terraform apply -no-color -input=false -auto-approve=true -lock=true /tmp/tmp70dp_ia7.tfplan", "outputs": {"ACI_TENANT_BROWNFOX_DESCRIPTION": {"sensitive": false, "type": "string", "value": "CHG0123456 by Joel W. King @joelwking"}, "ACI_TENANT_BROWNFOX_ID": {"sensitive": false, "type": "string", "value": "uni/tn-BROWNFOX"}, "ACI_USER_ACCOUNT_STATUS": {"sensitive": false, "type": "string", "value": "active"}, "Primary": {"sensitive": false, "type": "string", "value": "uni/infra/lldpIfP-lldp_primary"}, "Secondary": {"sensitive": false, "type": "string", "value": "uni/infra/lldpIfP-lldp_secondary"}, "VRF_ID": {"sensitive": false, "type": "string", "value": "uni/tn-BROWNFOX/ctx-GREEN"}, "VRF_NAME": {"sensitive": false, "type": "string", "value": "GREEN"}}, "state": "present", "stderr": "", "stderr_lines": [], "stdout": "Refreshing Terraform state in-memory prior to plan...\nThe refreshed state will be used to calculate this plan, but will not be\npersisted to local or remote state storage.\n\nlocal_file.foo: Refreshing state... [id=4bf3e335199107182c6f7638efaad377acc7f452]\ndata.aci_local_user.ACI_DATASOURCE: Refreshing state... [id=uni/userext/user-admin]\naci_tenant.FLINT: Refreshing state... [id=uni/tn-FLINT]\nmodule.lldp[\"primary\"].aci_lldp_interface_policy.lldp_policy: Refreshing state... [id=uni/infra/lldpIfP-lldp_primary]\nmodule.lldp[\"secondary\"].aci_lldp_interface_policy.lldp_policy: Refreshing state... [id=uni/infra/lldpIfP-lldp_secondary]\naci_tenant.BROWNFOX: Refreshing state... [id=uni/tn-BROWNFOX]\naci_vrf.GREEN: Refreshing state... [id=uni/tn-BROWNFOX/ctx-GREEN]\naci_vrf.insidevrf: Refreshing state... [id=uni/tn-BROWNFOX/ctx-inside_vrf]\naci_vrf.dmzvrf: Refreshing state... [id=uni/tn-BROWNFOX/ctx-DMZ_vrf]\n\n------------------------------------------------------------------------\n\nNo changes. Infrastructure is up-to-date.\n\nThis means that Terraform did not detect any differences between your\nconfiguration and real physical resources that exist. As a result, no\nactions need to be performed.\n", "stdout_lines": ["Refreshing Terraform state in-memory prior to plan...", "The refreshed state will be used to calculate this plan, but will not be", "persisted to local or remote state storage.", "", "local_file.foo: Refreshing state... [id=4bf3e335199107182c6f7638efaad377acc7f452]", "data.aci_local_user.ACI_DATASOURCE: Refreshing state... [id=uni/userext/user-admin]", "aci_tenant.FLINT: Refreshing state... [id=uni/tn-FLINT]", "module.lldp[\"primary\"].aci_lldp_interface_policy.lldp_policy: Refreshing state... [id=uni/infra/lldpIfP-lldp_primary]", "module.lldp[\"secondary\"].aci_lldp_interface_policy.lldp_policy: Refreshing state... [id=uni/infra/lldpIfP-lldp_secondary]", "aci_tenant.BROWNFOX: Refreshing state... [id=uni/tn-BROWNFOX]", "aci_vrf.GREEN: Refreshing state... [id=uni/tn-BROWNFOX/ctx-GREEN]", "aci_vrf.insidevrf: Refreshing state... [id=uni/tn-BROWNFOX/ctx-inside_vrf]", "aci_vrf.dmzvrf: Refreshing state... [id=uni/tn-BROWNFOX/ctx-DMZ_vrf]", "", "------------------------------------------------------------------------", "", "No changes. Infrastructure is up-to-date.", "", "This means that Terraform did not detect any differences between your", "configuration and real physical resources that exist. As a result, no", "actions need to be performed."], "workspace": "default"}

TASK [debug] *******************************************************************************************************************************
ok: [localhost] => {
    "msg": "the value of ACI_USER_ACCOUNT_STATUS is active"
}

PLAY RECAP *********************************************************************************************************************************
localhost                  : ok=2    changed=0    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0 
```

Review the playbook `terraform_main.yml` and note the debug task, which references an output variable defined in the `outputs.tf` file.


<p align="justify">
  # # #
</p>
