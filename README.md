Synopsis
--------
Terraform is an open-source infrastructure as code software tool created by HashiCorp. It is written in GoLang. Cisco has developed an ACI terraform provider is used to interact with the Cisco APIC. Network engineers define and provision the ACI infrastructure using a declarative configuration language known as HCL, HashiCorp Configuration Language. This repository is a demonstration of how to use Terraform to manage Cisco ACI.

README
------
This `README` file provides a learning (training) environment to become familiar with using Terraform to manage the configuration of a Cisco ACI fabric.  Assuming you have access to a compute resource with Vagrant and a supported virtualization environment (Virtual box), the configuration files included can be used as a basis for using Terraform with Cisco ACI. 

The Terraform configuration and commands have been tested using the Cisco DevNet Always-on ACI sandbox. Refer to [DevNet](https://developer.cisco.com/docs/aci/#!sandbox/aci-sandboxes). If you wish to use this sandbox for the exercise, obtain the sandbox APIC URL, userid and password by creating an account on Cisco DevNet and logging on to obtain these credentials.

Learning (Training) Environment
--------------------

Download the sample Vagrantfile to your laptop or development environment, Review the file at https://gitlab.com/joelwking/terraform_aci/-/blob/master/examples/training_environment/Vagrantfile.
```bash
wget https://gitlab.com/joelwking/terraform_aci/-/raw/master/examples/training_environment/Vagrantfile
```
Review and modify the IP addressing in the Vagrantfile and then issue `vagrant up`. After the virtual machine boots and is provisioned, issue `vagrant ssh` to connect to the virtual machine.  When complete, exit the VM shell and issue `vagrant destroy`.

Now, install Terraform.

Installing Terraform
--------------------
Installing Terraform, [download](https://www.terraform.io/downloads.html), unzip and install in `/usr/local/bin`. Verify your PATH environment variable contains the target directory, issue `$ env | grep PATH`.

>Note: if the program `unzip` is not installed, you can install it using the command `sudo apt install unzip`

```bash
$ cd /usr/local/bin
$ sudo wget https://releases.hashicorp.com/terraform/0.13.4/terraform_0.13.4_linux_amd64.zip
$ sudo unzip terraform_0.13.4_linux_amd64.zip

```
Verify the PATH and version of the executable.
```bash
$ cd ~
$ terraform --version
Terraform v0.13.4
```
Optionally remove the `.zip` file following installation
```bash
$ sudo rm /usr/local/bin/terraform_0.13.4_linux_amd64.zip
```

Ansible Integration
------------------
To illustrate the integration between Ansible and Terraform, the Vagrant provisioning step installs Ansible in a virtual environment. While Terraform is distributed as a binary, Ansible relies on Python and can be installed using PIP.

Activate the Python virtual environment.

```shell
source ansible210/bin/activate
```

In the `examples/ansible/playbooks` directory, there is a sample playbook which calls Terraform. The `local_exec.tf` file illustrates Terraform calling Ansible.

>Note: Simply move or delete the `local_exec.tf` file from the root directory `mv local_exec.tf /tmp/local_exec.tf` if you want to eliminate Terraform calling Ansible.

Clone repository
----------------
Clone this repository and enter the cloned directory.
```bash
$ git clone https://gitlab.com/joelwking/terraform_aci.git
$ cd terraform_aci
```
Specify credentials
-------------------
Send an environment variable with the password for the target APIC.
```bash
$ export TF_VAR_apic_password="changeme"
```

>Note: if you choose not use use a remote state file, simply move or delete the backend configuration file from the root directory `mv backend.tf /tmp/backend.tf`. You can then skip the following section and continue at the **Init** section. Read this [documentation](https://www.terraform.io/docs/language/state/remote.html) for information on remote state.

Configure the S3 backend
------------------------
For this exercise, the use of a remote [state](https://www.terraform.io/docs/state/purpose.html) is implemented by using the AWS S3 object store. This backend also supports state locking and consistency checking by optionally configuring Dynamo DB. 

**Create permissions and S3 bucket**

From the AWS dashboard under Identity and Access Management (IAM), create (or reference) a security group. In this example, we created a security group `terraform` with with `AmazonS3FullAccess` permissions.

**Create user**

From the AWS dashboard under Identity and Access Management (IAM), create (or reference) a user. In this example, we created a user `aci`. We selected `access type` *programmatic access*, and associated the user with group `terraform`.

> Note: Under the *Security Credentials* tab, you can create up to two access keys for the user. If you currently have two, you will need to delete existing key(s) to generate a new key.

Create a file named `credentials_backend` with the AWS S3 bucket access and secret key.
```
access_key =  "12345"
secret_key =  "12345"
```

**Create S3 bucket folders**

From the AWS dashboard under S3, create (or reference) a bucket.  In this example, we created a bucket 'terraform4567'. The bucket must be unique with region. Under the bucket, create a folder `network` and under that folder, create a second folder `aci`.

You do not need to create an object.

Update the `backend.tf` file with the bucket name and region.

Initializing the remote
-----------------------
Initializing will verify the configuration files, download the necessary providers, and initialize the *S3 backend* to maintain the state files. 

Use the command `terraform init` from your working directory. 

```bash
$ terraform init -backend-config=credentials_backend
```
Correct any errors identified by the `terraform init` command.

The output of a successful initialization follows:
```
Initializing modules...

Initializing the backend...

Successfully configured the backend "s3"! Terraform will automatically
use this backend unless the backend configuration changes.

Initializing provider plugins...
- Finding ciscodevnet/aci versions matching "~> 0.4.1"...
- Installing ciscodevnet/aci v0.4.1...
- Installed ciscodevnet/aci v0.4.1 (signed by a HashiCorp partner, key ID 433649E2C56309DE)

Partner and community providers are signed by their developers.
If you'd like to know more about provider signing, you can read about it here:
https://www.terraform.io/docs/plugins/signing.html

Terraform has been successfully initialized!
```

Init
----
To create the state file on the backend, initialize:

```bash
$ terraform init
```
You should see a message indicating `Initializing the backend...`. From the AWS console, you should see that the state file has been created.

Refresh
-------
Using the `terraform refresh` reads the current infrastructure configuration and updates the state file. Refer to the [refresh](https://www.terraform.io/docs/commands/refresh.html) documentation.

Following the successful execution, the S3 Backend will contain the current state. From the ASW management console, view the object in the backend S3 bucket. For example, the sample URL is:
```
Object URL https://wwt-gsd.s3.amazonaws.com/network/aci/state.tfstate
```
The state file is JSON formatted.

Show State
----------
Shows the attributes of a resource (or all resources) in the Terraform state.
```bash
$ terraform state show aci_tenant.BROWNFOX
```
The output can also be dumped as JSON:
```bash
$ terraform show -json | python -m json.tool
```

Plan
----
Issue the *plan* option to generate an execution [plan](https://www.terraform.io/docs/commands/plan.html). By default, Terraform will refresh the state file with the configuration of the target infrastructure.

By comparing the configuration files the the root directory with the refreshed state file, Terraform can determine what changes are necessary to achieve the desired state of the infrastrucure.

```bash
$ terraform plan
```
>Note: the *plan* option does not validate the input values.

Refer to the **Appendix** section for sample output from the `terraform plan`.

Apply
-----
Issue the `terraform apply` [*apply*](https://www.terraform.io/docs/commands/apply.html) option to apply the plan to the target device.

> Note, the *apply* invokes a *plan* generating an execution plan prior to the apply.

Importing Resources
-------------------
In this ACI use case, the configuration files only reference a small portion of the entire ACI Management Information Tree (MIT).  Existing configuration can be imported and managed.

Refer to the documentation on [importing resources](https://www.terraform.io/docs/import/index.html).

> Note: *The current implementation of Terraform import can only import resources into the state. It does not generate configuration. A future version of Terraform will also generate configuration.*

That means you must define the resource 'stub', import the resource into the state, then update the resource configuration manually (using your text editor). You can cut and paste the output of the `terraform show` command after you import the resource into the state file to update the 'stub' resource you created.

This is an example of importing a Tenant configuration.
```bash
terraform import aci_tenant.FLINT uni/tn-FLINT
```
```
Error: resource address "aci_tenant.FLINT" does not exist in the configuration.

Before importing this resource, please create its configuration in the root module. For example:

resource "aci_tenant" "FLINT" {
  # (resource arguments)
}
```
Added the above to the `resources.tf` file verbatim and issued the command again:

```bash
administrator@flint:~/sf/terraform/aci$ terraform import aci_tenant.FLINT uni/tn-FLINT
aci_tenant.FLINT: Importing from ID "uni/tn-FLINT"...
aci_tenant.FLINT: Import prepared!
  Prepared aci_tenant for import
aci_tenant.FLINT: Refreshing state... [id=uni/tn-FLINT]

Import successful!

The resources that were imported are shown above. These resources are now in
your Terraform state and will henceforth be managed by Terraform.

```

Following the import, the state file will contain:
```json
{
      "mode": "managed",
      "type": "aci_tenant",
      "name": "FLINT",
      "provider": "provider[\"registry.terraform.io/ciscodevnet/aci\"]",
      "instances": [
        {
          "schema_version": 1,
          "attributes": {
            "annotation": "",
            "description": "@joelwking Terraform testing",
            "id": "uni/tn-FLINT",
            "name": "FLINT",
            "name_alias": "GWP",
            "relation_fv_rs_tenant_mon_pol": null,
            "relation_fv_rs_tn_deny_rule": []
          },
          "private": "eyJzY2hlbWFfdmVyc2Q=="
        }
      ]
    },
```

Following the import, the `resources.tf` file has not been updated, it contains
```
resource "aci_tenant" "FLINT" {
  # (resource arguments)
}
```

Now update the resource definition:
```
resource "aci_tenant" "FLINT" {
  name                   =  "FLINT",
  name_alias             =  "German_Wirehaired_Pointer"
}
```

Modify the configuration
------------------------
Issue the `terraform apply` command to apply the modifications made to the configuration file.

Debugging
---------
Debugging output can be enabled: [debugging](https://www.terraform.io/docs/internals/debugging.html)

```bash
 export TF_LOG=DEBUG
```

Evaulate expressions interactively
----------------------------------
The terraform [console](https://www.terraform.io/docs/commands/console.html) command provides an interactive REPL console (Read-eval-print-loop) for evaluating expressions.

```bash
administrator@flint:~/sf/terraform/aci/terraform_aci$ terraform console
> local.initiator
Joel W. King @joelwking
> aci_tenant.FLINT
{
  "annotation" = "orchestrator:terraform"
  "description" = "CHG0123456 by Joel W. King @joelwking"
  "id" = "uni/tn-FLINT"
  "name" = "FLINT"
  "name_alias" = "GWP"
}
> aci_vrf.GREEN.id
uni/tn-BROWNFOX/ctx-GREEN
>
```

Inline usage: 

```bash
i$ echo "local.initiator" | terraform console
Joel W. King @joelwking
```

Destroy
-------
Use the `terraform destroy` command to [destroy](https://www.terraform.io/docs/commands/destroy.html) the managed infrastructure

Ansible calling Terraform
-------------------------

Issue the `terraform destroy` command to destroy the managed infrastructure. Then use the Ansible playbook provided to re-establish the infrastructure. Enter the playbook directory.

```bash
cd $HOME/terraform_aci/examples/ansible/playbooks
```

Execute the Ansible playbook, passing the extra vars `state` and `path`.

```bash
ansible-playbook  ./terraform_main.yml -v -e 'state=present path=$HOME/terraform_aci'
```

The output indicates the resources have been created and the output(s) from the Terraform module are passed back to the calling playbook.

Failure to Save State
---------------------
Connectivity issue may prevent saving a remote state. For example:

```
Failed to save state: failed to upload state: RequestError: send request failed
caused by: Put "https://wwt-gsd.s3.amazonaws.com/network/aci/state.tfstate": dial tcp: lookup wwt-gsd.s3.amazonaws.com on 127.0.0.53:53: no such host


Error: Failed to persist state to backend.

The error shown above has prevented Terraform from writing the updated state
to the configured backend. To allow for recovery, the state has been written
to the file "errored.tfstate" in the current working directory.

Running "terraform apply" again at this point will create a forked state,
making it harder to recover.

To retry writing this state, use the following command:
    terraform state push errored.tfstate

```
The local copy of the state file looks like this:
```bash
$ more errored.tfstate
{
  "version": 4,
  "terraform_version": "0.13.3",
  "serial": 22,
  "lineage": "dd1fa208-3145-b33d-68c5-00918a86ac4d",
  "outputs": {},
  "resources": []
}
```
Attempting to push the state `terraform state push errored.tfstate` shows:
```
administrator@flint:~/sf/terraform/aci$ terraform state push errored.tfstate

Error: error configuring S3 Backend: error validating provider credentials: error calling sts:GetCallerIdentity: RequestError: send request failed
caused by: Post "https://sts.amazonaws.com/": dial tcp: lookup sts.amazonaws.com on 127.0.0.53:53: no such host
```
The root problem was a failure to resolve DNS for the host.

Environment Variables
---------------------
Environment [variables](https://www.terraform.io/docs/commands/environment-variables.html) can be used as input data.

Examples:
```bash
export TF_VAR_region=us-west-1
export TF_VAR_ami=ami-049d8641
export TF_VAR_alist='[1,2,3]'
export TF_VAR_amap='{ foo = "bar", baz = "qux" }'
```

References
----------
References and perspective from other sources.

* ACI Provider: https://www.terraform.io/docs/providers/aci/index.html
* On Terraform code structure for scale https://sysdogs.com/on-terraform-code-structure-for-scale/
* Matt Mullen: https://github.com/mamullen13316/terraform_aci
* Tyler Hatton: https://github.com/tylerhatton/ansible-student-lab-tf-template
* vfifthfive: https://github.com/vfiftyfive/CLEUR19-Terraform
* Terraform 0.12 Deep Dive: HCL Enhancements for Infrastructure as Code, Remote Plan & Apply: https://www.hashicorp.com/resources/terraform-0-12-deep-dive-hcl-2-remote-plan-apply?source=tfosspromo

Author
------
Joel W. King, World Wide Technology @joelwking

Appendix
--------

## Output of `terraform plan`
```
vagrant@ubuntu-bionic:~/terraform_aci$ terraform plan
Refreshing Terraform state in-memory prior to plan...
The refreshed state will be used to calculate this plan, but will not be
persisted to local or remote state storage.

module.lldp["primary"].aci_lldp_interface_policy.lldp_policy: Refreshing state... [id=uni/infra/lldpIfP-lldp_primary]
aci_tenant.BROWNFOX: Refreshing state... [id=uni/tn-BROWNFOX]
module.lldp["secondary"].aci_lldp_interface_policy.lldp_policy: Refreshing state... [id=uni/infra/lldpIfP-lldp_secondary]
aci_tenant.FLINT: Refreshing state... [id=uni/tn-FLINT]
aci_vrf.GREEN: Refreshing state... [id=uni/tn-BROWNFOX/ctx-GREEN]
aci_vrf.dmzvrf: Refreshing state... [id=uni/tn-BROWNFOX/ctx-DMZ_vrf]
aci_vrf.insidevrf: Refreshing state... [id=uni/tn-BROWNFOX/ctx-inside_vrf]

------------------------------------------------------------------------

An execution plan has been generated and is shown below.
Resource actions are indicated with the following symbols:
  ~ update in-place

Terraform will perform the following actions:

  # aci_tenant.FLINT will be updated in-place
  ~ resource "aci_tenant" "FLINT" {
        annotation                  = "orchestrator:terraform"
        description                 = "CHG0123456 by Joel W. King @joelwking"
        id                          = "uni/tn-FLINT"
        name                        = "FLINT"
      ~ name_alias                  = "German_Wirehaired_Pointer" -> "GWP"
        relation_fv_rs_tn_deny_rule = []
    }

Plan: 0 to add, 1 to change, 0 to destroy.

------------------------------------------------------------------------

Note: You didn't specify an "-out" parameter to save this plan, so Terraform
can't guarantee that exactly these actions will be performed if
"terraform apply" is subsequently run.

```

## Output of `terraform apply`
```
administrator@flint:~/sf/terraform/aci$ terraform apply

An execution plan has been generated and is shown below.
Resource actions are indicated with the following symbols:
  + create

Terraform will perform the following actions:

  # aci_tenant.BROWNFOX will be created
  + resource "aci_tenant" "BROWNFOX" {
      + annotation  = "FOX"
      + description = "joelwking created by terraform"
      + id          = (known after apply)
      + name        = "BROWNFOX"
      + name_alias  = (known after apply)
    }

  # aci_vrf.GREEN will be created
  + resource "aci_vrf" "GREEN" {
      + annotation             = "tag_vrf"
      + bd_enforced_enable     = "no"
      + description            = (known after apply)
      + id                     = (known after apply)
      + ip_data_plane_learning = "enabled"
      + knw_mcast_act          = "permit"
      + name                   = "GREEN"
      + name_alias             = "alias_vrf"
      + pc_enf_dir             = "egress"
      + pc_enf_pref            = "unenforced"
      + tenant_dn              = (known after apply)
    }

  # aci_vrf.dmzvrf will be created
  + resource "aci_vrf" "dmzvrf" {
      + annotation             = "tag_vrf"
      + bd_enforced_enable     = "no"
      + description            = (known after apply)
      + id                     = (known after apply)
      + ip_data_plane_learning = "enabled"
      + knw_mcast_act          = "permit"
      + name                   = "demo_vrf"
      + name_alias             = "alias_vrf"
      + pc_enf_dir             = "egress"
      + pc_enf_pref            = "unenforced"
      + tenant_dn              = (known after apply)
    }

Plan: 3 to add, 0 to change, 0 to destroy.

Changes to Outputs:
  + ACI_TENANT_BROWNFOX_ID = (known after apply)

Do you want to perform these actions?
  Terraform will perform the actions described above.
  Only 'yes' will be accepted to approve.

  Enter a value: yes

aci_tenant.BROWNFOX: Creating...
aci_tenant.BROWNFOX: Creation complete after 2s [id=uni/tn-BROWNFOX]
aci_vrf.dmzvrf: Creating...
aci_vrf.GREEN: Creating...
aci_vrf.GREEN: Creation complete after 3s [id=uni/tn-BROWNFOX/ctx-GREEN]
aci_vrf.dmzvrf: Creation complete after 4s [id=uni/tn-BROWNFOX/ctx-demo_vrf]

Apply complete! Resources: 3 added, 0 changed, 0 destroyed.

Outputs:

ACI_TENANT_BROWNFOX_ID = uni/tn-BROWNFOX
```