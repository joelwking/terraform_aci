/*
    Copyright (c) 2020 World Wide Technology, LLC
    All rights reserved.
    
    author: Joel W. King  @joelwking

    usage:

      Demonstrate using a Data Source to return information from the ACI fabric.

    references:
      - https://registry.terraform.io/providers/CiscoDevNet/aci/latest/docs/data-sources/aaauser

*/

data "aci_local_user" "ACI_DATASOURCE" {
  name  = "admin"
}